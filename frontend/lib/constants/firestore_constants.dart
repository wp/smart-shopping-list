class FirestoreConstants {
  static const pathUserCollection = "users";
  static const displayName = "displayName";
  static const photoUrl = "photoUrl";
  static const id = "id";
  static const email = "email";
  static const groupId = "groupId";
}
