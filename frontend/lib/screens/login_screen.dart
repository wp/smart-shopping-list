import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:frontend/constants/color_constants.dart';
import 'package:frontend/constants/text_field_constants.dart';
import 'package:frontend/providers/auth_provider.dart';
import 'package:frontend/screens/home_screen.dart';
import 'package:provider/provider.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    final authProvider = Provider.of<LocalAuthProvider>(context);

    return Scaffold(
        body: Container(
      width: double.infinity,
      decoration: const BoxDecoration(color: Colors.white),
      child: Flex(
        direction: Axis.vertical,
        children: [
          Expanded(
              child: Container(
            decoration: const BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                      Color.fromRGBO(93, 226, 153, 0.8),
                      Color.fromRGBO(61, 220, 127, 1)
                    ]),
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.elliptical(250, 100),
                    bottomRight: Radius.elliptical(250, 100))),
            child: Center(
                child: Image.asset(
              "assets/images/food.png",
              width: 300,
            )),
          )),
          Expanded(
              child: Container(
            padding: const EdgeInsets.all(35),
            decoration: const BoxDecoration(color: Colors.white),
            child: Center(
              child: Flex(direction: Axis.vertical, children: [
                Container(
                  padding: const EdgeInsets.only(top: 20),
                  child: Column(children: [
                    const Text(
                      "Choose products that are good for you and planet",
                      textAlign: TextAlign.center,
                      style:
                          TextStyle(fontSize: 23, fontWeight: FontWeight.w700),
                    ),
                    const SizedBox(height: 20),
                    const Text(
                      "Our app is collaborative and open source project made by Faculty of Computer science and enginnering",
                      style: TextStyle(),
                      textAlign: TextAlign.center,
                    ),
                    Container(
                      padding: const EdgeInsets.symmetric(vertical: 20),
                      child: const Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.circle,
                            size: 15,
                            color: Color.fromARGB(255, 196, 196, 196),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Icon(
                            Icons.circle,
                            size: 15,
                            color: Color.fromRGBO(50, 185, 108, 1),
                          )
                        ],
                      ),
                    )
                  ]),
                ),
                Expanded(
                  child: Center(
                      child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      OutlinedButton(
                        style: ButtonStyle(
                            backgroundColor:
                                MaterialStateProperty.all<Color>(Colors.white),
                            padding: MaterialStateProperty.all(
                                const EdgeInsets.symmetric(
                                    horizontal: 13, vertical: 13))),
                        child: const Icon(
                          Icons.arrow_back,
                          color: Colors.black,
                        ),
                        onPressed: () {
                          Navigator.pop(context);
                        },
                      ),
                      const SizedBox(width: 20),
                      ElevatedButton(
                        style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all<Color>(
                                const Color.fromARGB(255, 34, 193, 103)),
                            padding: MaterialStateProperty.all(
                                const EdgeInsets.symmetric(
                                    horizontal: 25, vertical: 10))),
                        child: Row(
                          children: [
                            Image.asset(
                              "assets/images/google-icon.png",
                              width: 30,
                              height: 30,
                            ),
                            const SizedBox(
                              width: 5,
                            ),
                            const Text(
                              "Sign In",
                              style:
                                  TextStyle(color: Colors.white, fontSize: 20),
                            ),
                          ],
                        ),
                        onPressed: () async {
                          bool isSuccess = await authProvider.handleGoogleSignIn();
                          if (isSuccess) {
                            // ignore: use_build_context_synchronously
                            Navigator.pushReplacement(context,
                                MaterialPageRoute(builder: (context) => HomeScreen()));
                          }
                        },
                      ),
                    ],
                  )),
                )
              ]),
            ),
          ))
        ],
      ),
    ));
  }
}
