import 'package:flutter/material.dart';
import 'package:frontend/providers/auth_provider.dart';
import 'package:frontend/screens/home_screen.dart';
import 'package:frontend/screens/login_screen.dart';
import 'package:provider/provider.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);
  @override
  State<StatefulWidget> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>{

@override
  void initState() {
    super.initState();
    checkSignedIn();
  }

  void checkSignedIn() async {
    LocalAuthProvider authProvider = context.read<LocalAuthProvider>();
    bool isLoggedIn = await authProvider.isLoggedIn();
    if (isLoggedIn) {
      // ignore: use_build_context_synchronously
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => HomeScreen()));
      return;
    }
    // ignore: use_build_context_synchronously
    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (context) => const LoginScreen()));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      width: double.infinity,
      decoration: const BoxDecoration(color: Colors.white),
      child: Flex(
        direction: Axis.vertical,
        children: [
          Expanded(
              child: Container(
            decoration: const BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                      Color.fromRGBO(93, 226, 153, 0.8),
                      Color.fromRGBO(61, 220, 127, 1)
                    ]),
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.elliptical(250, 100),
                    bottomRight: Radius.elliptical(250, 100))),
            child: Center(
                child: Image.asset(
              "assets/images/logo.png",
              width: 300,
            )),
          )),
          Expanded(
              child: Container(
            padding: const EdgeInsets.all(35),
            decoration: const BoxDecoration(color: Colors.white),
            child: Center(
              child: Flex(direction: Axis.vertical, children: [
                Container(
                  padding: const EdgeInsets.only(top: 20),
                  child: Column(children: [
                    const Text(
                      "Choose products that are good for you and planet",
                      textAlign: TextAlign.center,
                      style:
                          TextStyle(fontSize: 23, fontWeight: FontWeight.w700),
                    ),
                    const SizedBox(height: 20),
                    const Text(
                      "Our app is collaborative and open source project made by Faculty of Computer science and enginnering",
                      style: TextStyle(),
                      textAlign: TextAlign.center,
                    ),
                    Container(
                      padding: const EdgeInsets.symmetric(vertical: 20),
                      child: const Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.circle,
                            size: 15,
                            color: Color.fromRGBO(50, 185, 108, 1),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Icon(
                            Icons.circle,
                            size: 15,
                            color: Color.fromARGB(255, 196, 196, 196),
                          )
                        ],
                      ),
                    )
                  ]),
                ),
                Expanded(
                  child: Center(
                      child: ElevatedButton(
                    style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all<Color>(
                            const Color.fromARGB(255, 34, 193, 103)),
                        padding: MaterialStateProperty.all(
                            const EdgeInsets.symmetric(
                                horizontal: 30, vertical: 10))),
                    child: const Text(
                      "Get started",
                      style: TextStyle(color: Colors.white, fontSize: 20),
                    ),
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const LoginScreen()));
                    },
                  )),
                )
              ]),
            ),
          ))
        ],
      ),
    ));
  }
}
