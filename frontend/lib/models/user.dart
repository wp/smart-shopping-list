import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

import '../constants/firestore_constants.dart';

class AppUser extends Equatable {
  final String id;
  final String displayName;
  final String email;
  final String photoUrl;
  final String groupId;

  const AppUser(
      {required this.id,
      required this.displayName,
      required this.photoUrl,
      required this.email,
      required this.groupId});

  Map<String, dynamic> toJson() => {
        FirestoreConstants.displayName: displayName,
        FirestoreConstants.photoUrl: photoUrl,
        FirestoreConstants.email: email,
        FirestoreConstants.groupId: groupId,
      };

  factory AppUser.fromDocument(DocumentSnapshot snapshot) {
    String photoUrl = "";
    String nickname = "";
    String email = "";
    String groupId = "";

    try {
      photoUrl = snapshot.get(FirestoreConstants.photoUrl);
      nickname = snapshot.get(FirestoreConstants.displayName);
      email = snapshot.get(FirestoreConstants.email);
      groupId = snapshot.get(FirestoreConstants.groupId);
    } catch (e) {
      if (kDebugMode) {
        print(e);
      }
    }
    return AppUser(
        id: snapshot.id,
        photoUrl: photoUrl,
        displayName: nickname,
        email: email,
        groupId: groupId);
  }
  
  @override
  // TODO: implement props
  List<Object?> get props => [id, photoUrl, displayName, email, groupId];
}
