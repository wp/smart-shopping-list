package mk.ukim.finki.smartshoppinglist.web.controller;

import mk.ukim.finki.smartshoppinglist.model.ShoppingList;
import mk.ukim.finki.smartshoppinglist.model.User;
import mk.ukim.finki.smartshoppinglist.service.ShoppingListService;
import mk.ukim.finki.smartshoppinglist.service.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/lists")
public class ShoppingListController {

    private final ShoppingListService shoppingListService;

    public ShoppingListController(ShoppingListService shoppingListService) {
        this.shoppingListService = shoppingListService;
    }

    @GetMapping
    public ResponseEntity<List<ShoppingList>> getAll() {
        List<ShoppingList> shoppingLists = shoppingListService.findAll();
        return ResponseEntity.ok(shoppingLists);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ShoppingList> getById(@PathVariable Long id) {
        ShoppingList shoppingList = shoppingListService.findById(id);
        if (shoppingList == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(shoppingList);
    }

    @PostMapping
    public ResponseEntity<ShoppingList> create(@RequestParam Long groupId, @RequestParam List<Long> productsId) {
        ShoppingList shoppingList = shoppingListService.create(groupId,productsId);
        return ResponseEntity.ok(shoppingList);
    }


    @PostMapping("/{id}")
    public ResponseEntity<ShoppingList> update(@PathVariable Long shoppingListId, @RequestParam Long groupId, @RequestParam List<Long> productsId) {
        ShoppingList shoppingList = shoppingListService.findById(shoppingListId);
        if (shoppingList == null) {
            return ResponseEntity.notFound().build();
        }
        ShoppingList updatedShoppingList = shoppingListService.update(shoppingListId, groupId, productsId);
        return ResponseEntity.ok(updatedShoppingList);
    }

    @PostMapping("/{id}/delete")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        ShoppingList shoppingList = shoppingListService.findById(id);
        if (shoppingList == null) {
            return ResponseEntity.notFound().build();
        }
        shoppingListService.delete(id);
        return ResponseEntity.ok(null);
    }

}
