package mk.ukim.finki.smartshoppinglist.service.impl;

import mk.ukim.finki.smartshoppinglist.model.Category;
import mk.ukim.finki.smartshoppinglist.model.Product;
import mk.ukim.finki.smartshoppinglist.model.exceptions.InvalidCategoryIdException;
import mk.ukim.finki.smartshoppinglist.repository.CategoryRepository;
import mk.ukim.finki.smartshoppinglist.repository.ProductRepository;
import mk.ukim.finki.smartshoppinglist.service.CategoryService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {

   private final CategoryRepository categoryRepository;

    public CategoryServiceImpl(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public List<Category> findAll() {
        return categoryRepository.findAll();
    }

    @Override
    public Category findById(Long id) {
        return categoryRepository.findById(id).orElseThrow(InvalidCategoryIdException::new);
    }

    @Override
    public Category save(Category category) {
        return categoryRepository.save(category);
    }

    @Override
    public Category update(Long id, Category category) {
        Category categoryOld = this.findById(id);
        categoryOld.setName(category.getName());
        return categoryRepository.save(categoryOld);
    }

    @Override
    public void delete(Long id) {
        Category category = this.findById(id);
        categoryRepository.delete(category);
    }
}
