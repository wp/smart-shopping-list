package mk.ukim.finki.smartshoppinglist.service;

import mk.ukim.finki.smartshoppinglist.model.Category;
import mk.ukim.finki.smartshoppinglist.model.Product;

import java.util.List;

public interface CategoryService {
    List<Category> findAll();

    Category findById(Long id);

    Category save(Category category);

    Category update(Long id, Category category);

    void delete(Long id);
}
