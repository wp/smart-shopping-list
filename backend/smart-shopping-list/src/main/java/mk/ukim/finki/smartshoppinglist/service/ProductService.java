package mk.ukim.finki.smartshoppinglist.service;

import mk.ukim.finki.smartshoppinglist.model.Category;
import mk.ukim.finki.smartshoppinglist.model.Product;

import java.util.List;

public interface ProductService {

    List<Product> findAll();

    Product findById(Long id);

    Product create(String name, String barcode, String ingredients, String productPhoto, String barcodePhoto,
                   String ingredientsPhoto, String label, List<Long> categories);
    Product update(Long id, String name, String barcode, String ingredients, String productPhoto, String barcodePhoto,
                   String ingredientsPhoto, String label, List<Long> categories);

    void delete(Long id);
}