package mk.ukim.finki.smartshoppinglist.service;

import mk.ukim.finki.smartshoppinglist.model.Group;
import mk.ukim.finki.smartshoppinglist.model.User;

import java.util.List;

public interface GroupService {

    public List<Group> findAllGroups();

    public Group findGroupById(Long id);

    public Group saveGroup(Group group);

    public void deleteGroup(Long id);
}
