package mk.ukim.finki.smartshoppinglist.model;

import jakarta.persistence.*;
import lombok.Data;

import java.util.List;

@Data
@Entity
@Table(name = "app_group")
public class Group {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long groupId;

    public Group(Long groupId) {
        this.groupId = groupId;
    }

    public Group() {
    }
}
