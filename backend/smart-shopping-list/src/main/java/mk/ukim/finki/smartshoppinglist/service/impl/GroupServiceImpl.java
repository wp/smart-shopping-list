package mk.ukim.finki.smartshoppinglist.service.impl;

import mk.ukim.finki.smartshoppinglist.model.Group;
import mk.ukim.finki.smartshoppinglist.model.exceptions.InvalidGroupIdException;
import mk.ukim.finki.smartshoppinglist.repository.GroupRepository;
import mk.ukim.finki.smartshoppinglist.service.GroupService;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class GroupServiceImpl implements GroupService {

    private final GroupRepository groupRepository;

    public GroupServiceImpl(GroupRepository groupRepository) {
        this.groupRepository = groupRepository;
    }

    @Override
    public List<Group> findAllGroups(){
        return groupRepository.findAll();
    }

    @Override
    public Group findGroupById(Long id) {
        return this.groupRepository.findById(id).orElseThrow(InvalidGroupIdException::new);
    }

    @Override
    public Group saveGroup(Group group) {
        return this.groupRepository.save(group);
    }

    @Override
    public void deleteGroup(Long id) {
        Group group = this.findGroupById(id);
        groupRepository.delete(group);
    }


}
