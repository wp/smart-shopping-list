package mk.ukim.finki.smartshoppinglist.web.controller;

import mk.ukim.finki.smartshoppinglist.model.Group;
import mk.ukim.finki.smartshoppinglist.service.GroupService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/groups")
public class GroupController {

    private final GroupService groupService;

    public GroupController(GroupService groupService) {
        this.groupService = groupService;
    }

    @GetMapping
    public ResponseEntity<List<Group>> findAllGroups() {
        List<Group> groups = groupService.findAllGroups();
        return ResponseEntity.ok(groups);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Group> findGroupById(@PathVariable Long id) {
        Group group = groupService.findGroupById(id);
        if (group == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(group);
    }

    @PostMapping
    public ResponseEntity<Group> createGroup(@RequestBody Group group) {
        Group savedGroup = groupService.saveGroup(group);
        return ResponseEntity.ok(savedGroup);
    }

    @PostMapping("/{id}/delete")
    public ResponseEntity<Void> deleteGroup(@PathVariable Long id) {
        Group group = groupService.findGroupById(id);
        if (group == null) {
            return ResponseEntity.notFound().build();
        }
        groupService.deleteGroup(id);
        return ResponseEntity.ok(null);
    }
}