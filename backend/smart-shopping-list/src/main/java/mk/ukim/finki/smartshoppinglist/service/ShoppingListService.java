package mk.ukim.finki.smartshoppinglist.service;

import mk.ukim.finki.smartshoppinglist.model.ShoppingList;
import java.util.List;

public interface ShoppingListService {

    List<ShoppingList> findAll();

    ShoppingList findById(Long id);

    ShoppingList create(Long groupId, List<Long> productsId);

    ShoppingList update(Long shoppingListId, Long groupId, List<Long> productsId);

    void delete(Long id);
}
