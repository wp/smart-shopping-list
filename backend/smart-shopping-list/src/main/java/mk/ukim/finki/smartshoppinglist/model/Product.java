package mk.ukim.finki.smartshoppinglist.model;

import jakarta.persistence.*;
import lombok.Data;

import java.util.List;

@Data
@Entity
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long productId;
    private String name;
    private String barcode;
    private String ingredients;
    private String productPhoto;
    private String barcodePhoto;
    private String ingredientsPhoto;
    private String label;

    @ManyToMany
    private List<Category> categories;

    public Product(Long productId, String name, String barcode, String ingredients, String productPhoto, String barcodePhoto, String ingredientsPhoto, String label, List<Category> categories) {
        this.productId = productId;
        this.name = name;
        this.barcode = barcode;
        this.ingredients = ingredients;
        this.productPhoto = productPhoto;
        this.barcodePhoto = barcodePhoto;
        this.ingredientsPhoto = ingredientsPhoto;
        this.label = label;
        this.categories = categories;
    }

    public Product(String name, String barcode, String ingredients, String productPhoto, String barcodePhoto, String ingredientsPhoto, String label, List<Category> categories) {
        this.name = name;
        this.barcode = barcode;
        this.ingredients = ingredients;
        this.productPhoto = productPhoto;
        this.barcodePhoto = barcodePhoto;
        this.ingredientsPhoto = ingredientsPhoto;
        this.label = label;
        this.categories = categories;
    }

    public Product() {
    }
}
