package mk.ukim.finki.smartshoppinglist.repository;

import mk.ukim.finki.smartshoppinglist.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

}
