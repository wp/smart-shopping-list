package mk.ukim.finki.smartshoppinglist.service.impl;

import mk.ukim.finki.smartshoppinglist.model.Group;
import mk.ukim.finki.smartshoppinglist.model.Product;
import mk.ukim.finki.smartshoppinglist.model.ShoppingList;
import mk.ukim.finki.smartshoppinglist.model.exceptions.InvalidGroupIdException;
import mk.ukim.finki.smartshoppinglist.model.exceptions.InvalidShoppingListIdException;
import mk.ukim.finki.smartshoppinglist.repository.GroupRepository;
import mk.ukim.finki.smartshoppinglist.repository.ProductRepository;
import mk.ukim.finki.smartshoppinglist.repository.ShoppingListRepository;
import mk.ukim.finki.smartshoppinglist.service.ShoppingListService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ShoppingListServiceImpl implements ShoppingListService {

    private final ProductRepository productRepository;
    private final ShoppingListRepository shoppingListRepository;
    private final GroupRepository groupRepository;

    public ShoppingListServiceImpl(ProductRepository productRepository,ShoppingListRepository shoppingListRepository,
                                   GroupRepository groupRepository) {
        this.productRepository = productRepository;
        this.shoppingListRepository = shoppingListRepository;
        this.groupRepository = groupRepository;
    }


    @Override
    public List<ShoppingList> findAll() {
        return shoppingListRepository.findAll();
    }

    @Override
    public ShoppingList findById(Long id) {
        return shoppingListRepository.findById(id).orElseThrow(InvalidShoppingListIdException::new);
    }

    @Override
    public ShoppingList create(Long groupId, List<Long> productsId) {
        Group group = groupRepository.findById(groupId).orElseThrow(InvalidGroupIdException::new);
        List<Product> products = productRepository.findAllById(productsId);
        ShoppingList shoppingList = new ShoppingList(group,products);
        return shoppingListRepository.save(shoppingList);
    }

    @Override
    public ShoppingList update(Long shoppingListId, Long groupId, List<Long> productsId) {
        ShoppingList shoppingList = this.findById(shoppingListId);

        Group group = groupRepository.findById(groupId).orElseThrow(InvalidGroupIdException::new);
        List<Product> products = productRepository.findAllById(productsId);

        shoppingList.setGroup(group);
        shoppingList.setProducts(products);
        return shoppingListRepository.save(shoppingList);
    }

    @Override
    public void delete(Long id) {
        ShoppingList shoppingList = this.findById(id);
        shoppingListRepository.delete(shoppingList);
    }
}
