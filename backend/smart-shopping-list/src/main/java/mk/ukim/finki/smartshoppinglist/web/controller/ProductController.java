package mk.ukim.finki.smartshoppinglist.web.controller;

import mk.ukim.finki.smartshoppinglist.model.Product;
import mk.ukim.finki.smartshoppinglist.service.ProductService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/products")
public class ProductController {
    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping
    public ResponseEntity<List<Product>> getAllProducts() {
        List<Product> products = productService.findAll();
        return ResponseEntity.ok(products);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Product> getProductById(@PathVariable Long id) {
        Product product = productService.findById(id);
        if (product == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(product);
    }

    @PostMapping
    public ResponseEntity<Product> createProduct(@RequestParam String name,@RequestParam String barcode,@RequestParam String ingredients,
                                                 @RequestParam String productPhoto, @RequestParam String barcodePhoto,
                                                 @RequestParam String ingredientsPhoto,@RequestParam String label,@RequestParam List<Long> categories) {
        Product savedProduct = productService.create(name, barcode, ingredients, productPhoto, barcodePhoto, ingredientsPhoto, label, categories);
        return ResponseEntity.ok(savedProduct);
    }


    @PostMapping("/{id}")
    public ResponseEntity<Product> updateProduct(@PathVariable Long id, @RequestParam String name,@RequestParam String barcode,@RequestParam String ingredients,
                                                 @RequestParam String productPhoto, @RequestParam String barcodePhoto,
                                                 @RequestParam String ingredientsPhoto,@RequestParam String label,@RequestParam List<Long> categories) {
        Product existingProduct = productService.findById(id);
        if (existingProduct == null) {
            return ResponseEntity.notFound().build();
        }
        Product updatedProduct = productService.update(id, name, barcode, ingredients, productPhoto, barcodePhoto, ingredientsPhoto, label, categories);
        return ResponseEntity.ok(updatedProduct);
    }

    @PostMapping("/{id}/delete")
    public ResponseEntity<Void> deleteProduct(@PathVariable Long id) {
        Product product = productService.findById(id);
        if (product == null) {
            return ResponseEntity.notFound().build();
        }
        productService.delete(id);
        return ResponseEntity.ok(null);
    }
}
