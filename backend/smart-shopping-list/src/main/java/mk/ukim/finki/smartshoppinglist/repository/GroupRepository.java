package mk.ukim.finki.smartshoppinglist.repository;

import mk.ukim.finki.smartshoppinglist.model.Group;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GroupRepository extends JpaRepository<Group, Long> {
}
