package mk.ukim.finki.smartshoppinglist.service.impl;

import mk.ukim.finki.smartshoppinglist.model.Group;
import mk.ukim.finki.smartshoppinglist.model.User;
import mk.ukim.finki.smartshoppinglist.model.exceptions.InvalidUserEmailException;
import mk.ukim.finki.smartshoppinglist.model.exceptions.InvalidUserIdException;
import mk.ukim.finki.smartshoppinglist.repository.UserRepository;
import mk.ukim.finki.smartshoppinglist.service.UserService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> findAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public User findUserById(Long id) {
        return userRepository.findById(id).orElseThrow(InvalidUserIdException::new);
    }

    @Override
    public User findUserByEmail(String email) {
        return userRepository.findByEmail(email).orElseThrow(InvalidUserEmailException::new);
    }

    @Override
    public User create(User user) {
        return userRepository.save(user);
    }

    @Override
    public User update(Long id, User user) {
        User userOld = this.findUserById(id);
        userOld.setName(user.getName());
        userOld.setLastName(user.getLastName());
        userOld.setEmail(user.getEmail());
        userOld.setGroup(user.getGroup());
        return userRepository.save(userOld);
    }

    @Override
    public void delete(Long id) {
        User user = this.findUserById(id);
        userRepository.delete(user);
    }
}
