package mk.ukim.finki.smartshoppinglist.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import lombok.Data;

@Data
@Entity
@Table(name = "app_user")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long userId;
    private String name;
    private String lastName;
    private String email;


    @ManyToOne()
    @JoinColumn(name = "group_id")
    private Group group;

    public User() {
    }

    public User(String name, String lastName, String email, Group group) {
        this.name = name;
        this.lastName = lastName;
        this.email = email;
        this.group = group;
    }
}
