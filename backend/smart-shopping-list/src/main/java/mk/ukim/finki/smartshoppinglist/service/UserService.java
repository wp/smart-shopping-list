package mk.ukim.finki.smartshoppinglist.service;

import mk.ukim.finki.smartshoppinglist.model.Group;
import mk.ukim.finki.smartshoppinglist.model.User;

import java.util.List;
import java.util.Optional;

public interface UserService {
    public List<User> findAllUsers();

    public User findUserById(Long id);

    public User findUserByEmail(String email);

    public User create(User user);

    public User update(Long id, User user);

    public void delete(Long id);

}
