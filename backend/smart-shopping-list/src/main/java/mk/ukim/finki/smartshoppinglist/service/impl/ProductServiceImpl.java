package mk.ukim.finki.smartshoppinglist.service.impl;

import mk.ukim.finki.smartshoppinglist.model.Category;
import mk.ukim.finki.smartshoppinglist.model.Product;
import mk.ukim.finki.smartshoppinglist.model.exceptions.InvalidProductIdException;
import mk.ukim.finki.smartshoppinglist.repository.CategoryRepository;
import mk.ukim.finki.smartshoppinglist.repository.ProductRepository;
import mk.ukim.finki.smartshoppinglist.service.ProductService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;
    private final CategoryRepository categoryRepository;

    public ProductServiceImpl(ProductRepository productRepository, CategoryRepository categoryRepository) {
        this.productRepository = productRepository;
        this.categoryRepository = categoryRepository;
    }

    @Override
    public List<Product> findAll() {
        return productRepository.findAll();
    }

    @Override
    public Product findById(Long id) {
        return productRepository.findById(id).orElseThrow(InvalidProductIdException::new);
    }

    @Override
    public Product create(String name, String barcode, String ingredients, String productPhoto, String barcodePhoto,
                          String ingredientsPhoto, String label, List<Long> categories) {
        List<Category> categoryList = categoryRepository.findAllById(categories);
        Product product = new Product(name, barcode, ingredients, productPhoto, barcodePhoto, ingredientsPhoto, label, categoryList);
        return productRepository.save(product);
    }

    @Override
    public Product update(Long id, String name, String barcode, String ingredients, String productPhoto, String barcodePhoto,
                          String ingredientsPhoto, String label, List<Long> categories) {
        Product product = this.findById(id);
        product.setName(name);
        product.setBarcode(barcode);
        product.setIngredients(ingredients);
        product.setProductPhoto(productPhoto);
        product.setBarcodePhoto(barcodePhoto);
        product.setIngredientsPhoto(ingredientsPhoto);
        product.setLabel(label);
        List<Category> categoryList = categoryRepository.findAllById(categories);
        product.setCategories(categoryList);
        return productRepository.save(product);
    }

    @Override
    public void delete(Long id) {
        Product product = this.findById(id);
        productRepository.delete(product);
    }
}
